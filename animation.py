import numpy as np
import FreeCAD as App
import tempfile
from PySide import QtCore
import FreeCADGui

class Animation(object):
    def __init__(self):
        self.timer=QtCore.QTimer()
        QtCore.QObject.connect(self.timer, QtCore.SIGNAL("timeout()"),self.UpdateScene)  		  		
        self.__dir__ = tempfile.gettempdir()
        self.y = 0
        self.y1 = 0
        self.auxplotval = 0

    def restore(self):#Restore all the bodies to their original placements and all the vectors to their original start and end points
        self.stop()#Stop the animation before restoring the scene
        self.ani.current_time = '0 s'
		#Restore all the bodies to their original placements:
        for x in range(0, self.number_of_bodies):
            self.objects[x].Placement = App.ActiveDocument.getObjectsByLabel("body: "+self.objects[x].Label)[0].Placement
            
        #Restores all the froce vectors: 
        for x in range(0, self.number_of_joints):
            jo = App.ActiveDocument.getObjectsByLabel('joint: '+str(x+1))[0]
            a = float(jo.absolute_position_X.split(" ")[0])*1000.0
            b = float(jo.absolute_position_Y.split(" ")[0])*1000.0
            c = float(jo.absolute_position_Z.split(" ")[0])*1000.0
            length = App.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4
            self.joint_force_vectors[x].Start = (a, b, c)
            self.joint_force_vectors[x].End = (a+length, b+length, c+length)
            self.joint_force_vectors[x].recompute()

		#Show all the elements that were hidden:
        App.ActiveDocument.getObjectsByLabel('Bodies')[0].Visibility = True  

        for x in range(0, self.number_of_bodies):
            App.ActiveDocument.getObjectsByLabel(self.objects[x].Label)[0].Visibility = False
              
        for obj in App.ActiveDocument.Objects:
            App.ActiveDocument.getObject(obj.Name).purgeTouched()#After changing the object´s visivility they are "touched". I purge the touched to avoid recomputing the objects after the animation.
            if obj.Label.startswith('joint: '):               
                FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = True# Make the joints visible again
                App.ActiveDocument.getObject(obj.Name).purgeTouched()#these become "touched" after making them visible. Untouch them...
                
        #App.activeDocument().recompute(None,True,True)

    def UpdateViews(self):#This method hides all the elements in the 3D scene that do not take place in the animation.
        App.ActiveDocument.getObjectsByLabel('Bodies')[0].Visibility = False
        
        for x in range(0, self.number_of_bodies):
            App.ActiveDocument.getObjectsByLabel(self.objects[x].Label)[0].Visibility = True

        
        for obj in App.ActiveDocument.Objects:
            if obj.Label.startswith('joint: '):
                FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = False

                if obj.animate=="true":
                    App.ActiveDocument.getObjectsByLabel('jf: '+obj.label)[0].Visibility = True
                    

    def retrieve(self):#This method gets all the information needed in order to start the animation
        self.ani = App.ActiveDocument.getObject("Animation")#Get the animation scripted object, so that the current animation time can be show
        self.precission = int(App.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Get the precision, to round the current time.
        self.timestep = float(App.ActiveDocument.getObject("MBDyn").time_step[:-2])#Get the time-step. To calculat/display the current animation time
        self.y = 0 #This variable controls the current animation time (see the "updateBodies" method  
        self.y1 = 0 #Same as y but for joints
        self.data = np.genfromtxt(self.__dir__ + '/MBDynCase.mov', delimiter=' ')#Get the MBDY animation results
        self.bodies = self.data[:,0] #Obtain the first colum, which contains the bodiese´s numbers
        self.number_of_bodies = int(np.max(self.bodies)) #Count the number of bodies (each simulation must have at least one joint)
        self.number_of_joints = 0
        for obj in App.ActiveDocument.Objects:
            if(obj.Label.startswith('joint:')):#Count number of joints. This has to be counted from the FeeCAD document because some animations may not have any joint, so that the "MBDynCase.jnt" file may not exist or may correspond to another simulation             
                self.number_of_joints = self.number_of_joints + 1 

		#If there are joints, retrieve joint's data:
        self.joints = [] #An array to store all the joints in the FreeCAD model
        self.joint_force_vectors = [] #An array to store all the joint vectors sepaartely, for the animation
        if(self.number_of_joints>0):
            self.data1 = np.genfromtxt(self.__dir__ + '/MBDynCase.jnt', delimiter=' ',  usecols=(0,1,2,3,7,8,9))#get only the joint labels and the reaction forces. See: https://www.sky-engin.jp/en/MBDynTutorial/chap16/chap16.html
            for x in range(1, self.number_of_joints+1):#Store all the force vectors in the corresponding arrays:
                aux = App.ActiveDocument.getObjectsByLabel('jf: '+str(x))[0]
                aux1 = App.ActiveDocument.getObjectsByLabel('joint: '+str(x))[0]
                self.joint_force_vectors.append(aux)#Store the joint reation-force vectors
                self.joints.append(aux1)#Store the joints themselves

        self.times = len(self.data) #Count the number of discrete times or discrete positions that the animation has. This number is multiplied by the number of bodies.
        self.objects = [] #An array to store all the solid objects in the FreeCAD model
        self.rotation_centers = [] #An array to store all the initial node´s initial positions, which are used as the rotation centers in the animation, and to restore the scene after the animation
        self.placements = []#An array to store the inital placement of each body. This is used both, to restore the scene after the animation, and to move the bodies relatve to their original placements.
        self.initial_orientations = []#Array to store the initial orientations of all the bodies, so that the animation is performed relative to these initial orientations.
        for x in range(1, self.number_of_bodies+1):#Store all the bodies in the corresponding arrays:
            aux = App.ActiveDocument.getObjectsByLabel(str(x))[0]
            aux1 = App.ActiveDocument.getObjectsByLabel('structural: '+str(x))[0]
            self.objects.append(aux)
            self.placements.append(aux.Placement)
            self.rotation_centers.append(np.array([aux1.position_X.Value, aux1.position_Y.Value, aux1.position_Z.Value]))
            self.initial_orientations.append(np.array([aux1.yaw.Value, aux1.pitch.Value, aux1.roll.Value]))

    def start(self):#Call this method to start the animation. MBDyn has to be executed befor starting the animation, otherwhise there will not be results to animate.
        self.retrieve()
        self.UpdateViews()
        speed = float(App.ActiveDocument.getObject("Animation").speed)
        self.timer.start(speed)

    def stop(self):#This method stops the animation		
        self.timer.stop()  

    def UpdateScene(self):	#This method is connected with the timer, so that this method performs the animation itself. Call the retrieve() method before this one.	
        if(self.y>=self.times):
            self.timer.stop()
        else:
            self.ani.current_time = str(round((self.y/self.number_of_bodies)*self.timestep, self.precission))+' s'#Show the current time in the animation scripted object                
            positions_at_current_time = []
            #Animate the bodies:
            for x in range(0, self.number_of_bodies):#Update the position of all the bodies at once:                
                position = App.Vector((self.data[self.y][1]*1000)-self.rotation_centers[x][0], (self.data[self.y][2]*1000)-self.rotation_centers[x][1], (self.data[self.y][3]*1000)-self.rotation_centers[x][2])	
                positions_at_current_time.append([self.data[self.y][1]*1000,self.data[self.y][2]*1000,self.data[self.y][3]*1000])
                rotation = App.Rotation(self.data[self.y][4]-self.initial_orientations[x][0], self.data[self.y][5]-self.initial_orientations[x][1], self.data[self.y][6]-self.initial_orientations[x][2])	
                rotation_center = App.Vector(self.rotation_centers[x][0], self.rotation_centers[x][1], self.rotation_centers[x][2])	
                self.objects[x].Placement = App.Placement(position, rotation, rotation_center).multiply(self.placements[x])
                self.y = self.y + 1
    
            self.y = self.y + self.number_of_bodies
            
    		#Joint force vectors:
            for x in range(0, self.number_of_joints):#Update all the force vectors:                        
                if(self.joints[x].animate=='true'):
                    #These joints do not follow any moving body in the 3D scene, thus, I do not have to change their start point:
                    if(self.joints[x].joint == "revolute pin" or self.joints[x].joint == "clamp" or self.joints[x].joint == "drive hinge" or self.joints[x].joint == "axial rotation" or self.joints[x].joint == "prismatic" or self.joints[x].joint == "deformable displacement joint"):
                        if(self.joints[x].frame == "global"):
                            a = self.joint_force_vectors[x].Start[0] + self.data1[self.y1][4] * float(self.joints[x].force_vector_multiplier)
                            b = self.joint_force_vectors[x].Start[1] + self.data1[self.y1][5] * float(self.joints[x].force_vector_multiplier)
                            c = self.joint_force_vectors[x].Start[2] + self.data1[self.y1][6] * float(self.joints[x].force_vector_multiplier)
                        if(self.joints[x].frame == "local"):
                            a = self.joint_force_vectors[x].Start[0] + self.data1[self.y1][1] * float(self.joints[x].force_vector_multiplier)
                            b = self.joint_force_vectors[x].Start[1] + self.data1[self.y1][2] * float(self.joints[x].force_vector_multiplier)
                            c = self.joint_force_vectors[x].Start[2] + self.data1[self.y1][3] * float(self.joints[x].force_vector_multiplier)
                            
                        end_point = App.Vector(a, b, c)
                        self.joint_force_vectors[x].End = end_point
                        self.joint_force_vectors[x].recompute()
                        
                    #These vectors do follow a node in the 3D scene:    
                    if(self.joints[x].joint == "in line" or self.joints[x].joint == "revolute hinge" or self.joints[x].joint == "spherical hinge"):
                        px = positions_at_current_time[int(self.joints[x].structural_dummy)-1][0]#App.ActiveDocument.getObjectsByLabel(str(self.joints[x].structural_dummy))[0].Placement.Base[0] #App.ActiveDocument.getObjectsByLabel(str(self.joints[x].structural_dummy))[0].Shape.Solids[0].CenterOfMass[0]
                        py = positions_at_current_time[int(self.joints[x].structural_dummy)-1][1]#App.ActiveDocument.getObjectsByLabel(str(self.joints[x].structural_dummy))[0].Placement.Base[1] #App.ActiveDocument.getObjectsByLabel(str(self.joints[x].structural_dummy))[0].Shape.Solids[0].CenterOfMass[1]
                        pz = positions_at_current_time[int(self.joints[x].structural_dummy)-1][2]#App.ActiveDocument.getObjectsByLabel(str(self.joints[x].structural_dummy))[0].Placement.Base[2] #App.ActiveDocument.getObjectsByLabel(str(self.joints[x].structural_dummy))[0].Shape.Solids[0].CenterOfMass[2]                    
                        if(self.joints[x].frame == "global"):
                            a = self.joint_force_vectors[x].Start[0] + self.data1[self.y1][4] * float(self.joints[x].force_vector_multiplier)
                            b = self.joint_force_vectors[x].Start[1] + self.data1[self.y1][5] * float(self.joints[x].force_vector_multiplier)
                            c = self.joint_force_vectors[x].Start[2] + self.data1[self.y1][6] * float(self.joints[x].force_vector_multiplier)
                        if(self.joints[x].frame == "local"):
                            a = self.joint_force_vectors[x].Start[0] + self.data1[self.y1][1] * float(self.joints[x].force_vector_multiplier)
                            b = self.joint_force_vectors[x].Start[1] + self.data1[self.y1][2] * float(self.joints[x].force_vector_multiplier)
                            c = self.joint_force_vectors[x].Start[2] + self.data1[self.y1][3] * float(self.joints[x].force_vector_multiplier)
                        start_point = App.Vector(px, py, pz)  
                        end_point = App.Vector(a, b, c)
                        self.joint_force_vectors[x].Start = start_point
                        self.joint_force_vectors[x].End = end_point
                        self.joint_force_vectors[x].recompute()
                
                
                self.y1 = self.y1 + 1
    
            self.y1 = self.y1 + self.number_of_joints
    
    

