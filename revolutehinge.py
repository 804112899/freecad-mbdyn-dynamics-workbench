# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This scripted object represents a revolute hinge. See: https://www.sky-engin.jp/en/MBDynTutorial/chap14/chap14.html

The syntax is: 

joint: <label>, 
      revolute hinge, 
         <node 1>,
            <relative offset 1>,
            euler, 0.,0.,0., #<relative orientation matrix>
         <node 2>,
            <relative offset 2>,
            euler, 0.,0.,0., #<relative orientation matrix>;

label: an integer number to identify the joint, eg: 1,2,3... 
node1: the label of the first structural node to which the joint is attached, eg: 1,2,3... 
relative offset: the possition of the joint relative to it's structural node. For the example in the above web page is '-0.5, 0.0, 0.0', because the node is at '0.5, 0.0, 0.0' relative to the absolute origin

Example:

joint: 2, 
      revolute hinge, 
         1,                                      # first node or body
            0.5, 0., 0.,                         # relative offset
            hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
         2,                                      # second node or body
            0., 0., -0.5,                        # relative offset
            hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation
       
       relative axis orientation and absolute pin orientation
       to rotate around x axis: euler, 0., pi/2., 0.
       to rotate around y axis: euler, pi/2., 0., 0.
'''

#from FreeCAD import Units
import FreeCAD
from sympy import Point3D, Line3D
import Draft

class Revolutehinge:
    def __init__(self, obj, label, node1, node2, cylinder):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        #Get the cylinder's center of mass, so that the joint is placed here:
        x = FreeCAD.Units.Quantity(cylinder.Shape.CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        y = FreeCAD.Units.Quantity(cylinder.Shape.CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        z = FreeCAD.Units.Quantity(cylinder.Shape.CenterOfMass[2],FreeCAD.Units.Unit('mm')) 
        
        #Calculate the relative offset 1:       
        x1 = x-node1.position_X
        y1 = y-node1.position_Y
        z1 = z-node1.position_Z
        
        #Calculate the relative offset 2:       
        x2 = x-node2.position_X
        y2 = y-node2.position_Y
        z2 = z-node2.position_Z

        obj.addExtension("App::GroupExtensionPython", self)          
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Revolute hinge","label",1).label = label        
        obj.addProperty("App::PropertyString","node 1","Revolute hinge","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","Revolute hinge","node 2",1).node_2 = node2.label
        obj.addProperty("App::PropertyString","joint","Revolute hinge","joint",1).joint = 'revolute hinge'

        #Absolute pin position:                
        obj.addProperty("App::PropertyString","absolute position X","Absolute pin position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","Absolute pin position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","Absolute pin position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"
        
        #Relative offset 1:          
        obj.addProperty("App::PropertyString","relative offset 1 X","Relative offset 1","relative offset 1 X",1).relative_offset_1_X = str(round(x1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Y","Relative offset 1","relative offset 1 Y",1).relative_offset_1_Y = str(round(y1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Z","Relative offset 1","relative offset 1 Z",1).relative_offset_1_Z = str(round(z1.getValueAs('m').Value,precission))+" m"
        
        #Relative offset 2: 
        obj.addProperty("App::PropertyString","relative offset 2 X","Relative offset 2","relative offset 2 X",1).relative_offset_2_X = str(round(x2.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 2 Y","Relative offset 2","relative offset 2 Y",1).relative_offset_2_Y = str(round(y2.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 2 Z","Relative offset 2","relative offset 2 Z",1).relative_offset_2_Z = str(round(z2.getValueAs('m').Value,precission))+" m"
            
        obj.addProperty("App::PropertyString","orientation matrix 1","Revolute hinge","orientation matrix 1",1).orientation_matrix_1 = "3, 0.0, 0.0, 1.0, 2, guess"
        obj.addProperty("App::PropertyString","orientation matrix 2","Revolute hinge","orientation matrix 2",1).orientation_matrix_2 = "3, 0.0, 0.0, 1.0, 2, guess"
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyString","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = '1'
        
        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '1'

        obj.Proxy = self
        
        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        length = (cylinder.Shape.BoundBox.XLength+cylinder.Shape.BoundBox.YLength+cylinder.Shape.BoundBox.ZLength)/6 # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)
        #Add x vector of the coordinate system:
        p2 = FreeCAD.Vector(length, 0, 0)
        p2 = FreeCAD.Vector(0, 0, 0+2*length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True        
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/30)+' mm' 
        #l.ViewObject.Selectable = False              
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)    
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label  
        #d.ViewObject.Selectable = False 

        FreeCAD.ActiveDocument.recompute()                                                    
        
    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        
        ##############################################################################Calculate the new orientation: 
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line        
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:
        fp.orientation_matrix_1 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                
        fp.orientation_matrix_2 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                       
        
        ##############################################################################Recalculate the offset, in case any of the two nodes was moved: 
        #get the pin position (the pin position is not expected to change):
        x = float(fp.absolute_position_X.split(" ")[0])  
        y = float(fp.absolute_position_Y.split(" ")[0])
        z = float(fp.absolute_position_Z.split(" ")[0])
        
        #get the node´s position:
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
        
        #Re-calculate the joint possition relative to it's nodes (relative offset)        
        x1 = x - node1.position_X.getValueAs("m").Value
        y1 = y - node1.position_Y.getValueAs("m").Value
        z1 = z - node1.position_Z.getValueAs("m").Value
        
        x2 = x - node2.position_X.getValueAs("m").Value
        y2 = y - node2.position_Y.getValueAs("m").Value
        z2 = z - node2.position_Z.getValueAs("m").Value

        #Update the offset:
        fp.relative_offset_1_X = str(round(x1,precission))+" m"
        fp.relative_offset_1_Y = str(round(y1,precission))+" m"
        fp.relative_offset_1_Z = str(round(z1,precission))+" m"
        
        fp.relative_offset_2_X = str(round(x2,precission))+" m"
        fp.relative_offset_2_Y = str(round(y2,precission))+" m"
        fp.relative_offset_2_Z = str(round(z2,precission))+" m"

        FreeCAD.Console.PrintMessage("REVOLUTE HINGE JOINT: " +fp.label+" successful recomputation...\n")