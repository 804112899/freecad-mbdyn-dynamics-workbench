# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This class implements an in-line joint between two nodes. It receives an integer number which is used to label the joint, and two nodes. The second node is then constrained to a line fixed to the first node.

An in-line joint forces a point relative to the second node to move along a line attached to the first node.
A point, optionally offset by "relative offset" from the position of "node 2", slides along a
line that passes through a point that is rigidly offset by "relative_line_position" from the position of
"node 1", and is directed as direction 3 (the Z axis) of "relative orientation". 

The joint is defined as:

joint: <label>, 
       in line, 
       <node 1>, # node 2 is fixed to a line attached to node 1
       <relative line position>, #line position relative to node 1
       <relative orientation>, #line orientation is the orientation of the Z axis of the node's coordinate sistem
       <node 2>, #this node is attached to node 1
       [offset, <relative offset> ]; #if there is an offset between node 2 and the line.

'''

#from FreeCAD.Units.Units import FreeCAD.Units.Unit,FreeCAD.Units.Quantity
import FreeCAD
import Draft
from sympy import Point3D, Line3D
#import FreeCADGui

class Inline:
    def __init__(self, obj, label, node1, node2):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)

        x = node1.position_X
        y = node1.position_Y
        z = node1.position_Z
     
        obj.addExtension("App::GroupExtensionPython", self)  
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","In line joint","label",1).label = label
        obj.addProperty("App::PropertyString","joint","In line joint","joint",1).joint = 'in line'
        obj.addProperty("App::PropertyString","node 1","In line joint","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","In line joint","node 2",1).node_2 = node2.label
        
        #The absolute position is set at the node 1 position, only for animation, not for MBDyn sumulation:        
        obj.addProperty("App::PropertyString","absolute position X","Initial absolute position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","Initial absolute position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","Initial absolute position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m" 

        #The relative line position is initially set to (0,0,0), this is, the line passes through node 1
        obj.addProperty("App::PropertyString","relative line position X","Line position relative to node 1","relative line position X",1).relative_line_position_X = '0 m'
        obj.addProperty("App::PropertyString","relative line position Y","Line position relative to node 1","relative line position Y",1).relative_line_position_Y = '0 m'
        obj.addProperty("App::PropertyString","relative line position Z","Line position relative to node 1","relative line position Z",1).relative_line_position_Z = '0 m'
        
        #The relative line orientation is initially set to 3, 0.0, 0.0, 1.0, 2, guess, this is, the line goes paralel to the global Z axis.         
        obj.addProperty("App::PropertyString","orientation matrix","Relative line orientation","orientation matrix",1).orientation_matrix = '3, 0.0, 0.0, 1.0, 2, guess'        
        
        #The relative offset is initially set to (0,0,0), this is, node 2 is at the line, without offset
        obj.addProperty("App::PropertyString","relative offset X","Line offset relative to node 2","relative offset X",1).relative_offset_X = '0 m'
        obj.addProperty("App::PropertyString","relative offset Y","Line offset relative to node 2","relative offset Y",1).relative_offset_Y = '0 m'
        obj.addProperty("App::PropertyString","relative offset Z","Line offset relative to node 2","relative offset Z",1).relative_offset_Z = '0 m'
        
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']  
        
        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '2'
        
        obj.addProperty("App::PropertyString","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = '1'

        obj.Proxy = self
        
        length = FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: " + node1.label)[0].Length.Value # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)

        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(0, 0, 2*length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True        
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        #l.ViewObject.DrawStyle = u"Dashed"
        l.ViewObject.ArrowSize = str(length/30)+' mm' 
        #l.ViewObject.Selectable = False             
        
        #Add the vector to visualize reaction forces        
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        #d.ViewObject.Selectable = False
        d.Label = "jf: "+ label                    
                       
        FreeCAD.ActiveDocument.recompute()
        
    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        #A line that defines the joint:
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:
        fp.orientation_matrix = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                
        #Get the joint´s nodes:
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
        
        p3 = Point3D(node2.position_X.Value, node2.position_Y.Value, node2.position_Z.Value)
        
        if (node1.position_X.Value == node2.position_X.Value and node1.position_Y.Value == node2.position_Y.Value and node1.position_Z.Value == node2.position_Z.Value) or (p3 in l1):
            
            fp.relative_offset_X = "0.0 m"
            fp.relative_offset_Y = "0.0 m"
            fp.relative_offset_Z = "0.0 m"
            
        else:    
        
            #Get the position of the nodes:
            #p_node1 = Point3D(node1.position_X.Value, node1.position_Y.Value, node1.position_Z.Value)    
            p_node2 = Point3D(node2.position_X.Value, node2.position_Y.Value, node2.position_Z.Value)
            #Calculate a perpendicular line to obtain the line offset relative to node2:
            l2 = l1.perpendicular_line(p_node2)
            #Calculate a perpendicular line to obtain the line position relative to node1:
            #l3 = l1.perpendicular_line(p_node1)
            #Update the relative offset:
            relative_offset =  FreeCAD.Vector(float(l2.p2[0]),float(l2.p2[1]),float(l2.p2[2])) - FreeCAD.Vector(float(l2.p1[0]),float(l2.p1[1]),float(l2.p1[2]))         
            fp.relative_offset_X = str(round(relative_offset[0]/1000.0,precission))+" m"
            fp.relative_offset_Y = str(round(relative_offset[1]/1000.0,precission))+" m"
            fp.relative_offset_Z = str(round(relative_offset[2]/1000.0,precission))+" m"
            #Update the relative line position:
            
            #if float(l1.distance(p_node1))!=0:                
            #    relative_line_position =  FreeCAD.Vector(float(l3.p2[0]),float(l3.p2[1]),float(l3.p2[2])) - FreeCAD.Vector(float(l3.p1[0]),float(l3.p1[1]),float(l3.p1[2]))         
            #    fp.relative_line_position_X = str(relative_line_position[0]/1000.0)+" m"
            #    fp.relative_line_position_Y = str(relative_line_position[1]/1000.0)+" m"
            #    fp.relative_line_position_Z = str(relative_line_position[2]/1000.0)+" m"
        
        FreeCAD.Console.PrintMessage("IN-LINE JOINT: " +fp.label+" successful recomputation...\n")                                  
                                  
                                        