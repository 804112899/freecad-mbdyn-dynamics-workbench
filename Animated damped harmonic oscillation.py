import matplotlib.pyplot as plt
import numpy as np
#plt.rc("text", usetex=True)
plt.rc("font", size=20)

A = 10 #Initial amplitude
d = 15 #Proportional to the damper constant
s = 2.0 #Proportional to the spring constant
finaltime = 10#The final time (x axis)
time = np.linspace(0, finaltime, 500)#Create a times array
y = A*np.exp(d*-time)*np.cos(s*np.pi*time)#The damped harmonic socillation function

fig = plt.figure()
ax = fig.add_subplot(111)
line1, = ax.plot(time, y, label=r"$d=$"+str(d))
plt.xlabel(r"$t$",fontsize=35)
plt.ylabel(r"$y(t)=A \cdot e^{d \cdot -t} \cdot cos(s \cdot \pi \cdot t)$",fontsize=35)
plt.title("Damped harmonic oscillation under different damping constants")
ax.set_ylim([-10,10])

plt.ion()#Turn the interactive mode on
while (d>0.5):#Update the plot while the damping constant is higher that 0.5        
    d=d-0.1#Reduce the damping constant
    y = A*np.exp(d*-time)*np.cos(s*np.pi*time)#Calculate the function with the new spring constant
    line1.set_ydata(y)
    line1.set_label(r"$d=$"+str(d)[:4])
    fig.canvas.draw()
    plt.legend(loc=1)
    plt.grid(True)
    plt.pause(0.01)#Pause the graph so it can be visualized
   
plt.pause(5)#Add a pause before closing the graph
